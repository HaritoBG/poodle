﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Poodle_e_Learning_Platform.Migrations
{
    public partial class courseImageTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CoursePictures",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    CourseId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    OriginalName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OriginalType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UploadNumber = table.Column<int>(type: "int", nullable: false),
                    OriginalContent = table.Column<byte[]>(type: "varbinary(max)", nullable: true),
                    VisualizeContent = table.Column<byte[]>(type: "varbinary(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoursePictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CoursePictures_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CoursePictures_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CoursePictures_CourseId",
                table: "CoursePictures",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_CoursePictures_UserId",
                table: "CoursePictures",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoursePictures");
        }
    }
}
