﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Poodle_e_Learning_Platform.Migrations
{
    public partial class ImageDataThumbnailColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "ThumbnailContent",
                table: "Images",
                type: "varbinary(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ThumbnailContent",
                table: "Images");
        }
    }
}
