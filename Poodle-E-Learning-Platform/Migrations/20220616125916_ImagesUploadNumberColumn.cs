﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Poodle_e_Learning_Platform.Migrations
{
    public partial class ImagesUploadNumberColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UploadNumber",
                table: "Images",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UploadNumber",
                table: "Images");
        }
    }
}
