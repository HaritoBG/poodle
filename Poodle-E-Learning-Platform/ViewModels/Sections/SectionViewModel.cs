﻿namespace Poodle_e_Learning_Platform.ViewModels.Sections
{
    public class SectionViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }
    }
}
