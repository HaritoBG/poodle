﻿using System.Collections.Generic;

namespace Poodle_e_Learning_Platform.ViewModels.Home
{
    public class IndexViewModel
    {
        public int PagesCount { get; set; }
        //skip/take not used 
        public int Skip { get; set; }

        public int Take { get; set; }

        public int CurrentPage { get; set; }

        public int NextPage => CurrentPage + 1;

        public int PrevPage => CurrentPage - 1;

        public IEnumerable<IndexItemViewModel> Items { get; set; }
    }
}
