﻿using System;

namespace Poodle_e_Learning_Platform.ViewModels.Home
{
    public class IndexItemViewModel
    {
        public int Id { get; set; } //??
        public string Title { get; set; }

        public string Content { get; set; }

        public string ImagePath { get; set; }
    }
}
