﻿namespace Poodle_e_Learning_Platform.ViewModels.Student
{
    public class StudentViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public int RoleId { get; set; }

        public bool IsEnrolled { get; set; }
    }
}
