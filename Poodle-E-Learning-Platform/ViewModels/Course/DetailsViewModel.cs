﻿using Poodle_e_Learning_Platform.InputModels.Section;
using Poodle_e_Learning_Platform.ViewModels.Sections;
using Poodle_e_Learning_Platform.ViewModels.Student;
using System;
using System.Collections.Generic;

namespace Poodle_e_Learning_Platform.ViewModels.Course
{
    public class DetailsViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsPrivate { get; set; }

        public SectionInputModel SectionInputModel { get; set; }

        public IEnumerable<SectionViewModel> Sections { get; set; }
    }
}
