﻿using Poodle_e_Learning_Platform.ViewModels.Student;
using System.Collections.Generic;

namespace Poodle_e_Learning_Platform.ViewModels.Course
{
    public class EnrollViewModel
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public IEnumerable<StudentViewModel> Students { get; set; }
    }
}
