﻿using Poodle_e_Learning_Platform.Data;
using Poodle_e_Learning_Platform.Exceptions;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository.Contracts;
using System.Linq;

namespace Poodle_e_Learning_Platform.Repository
{
    public class EnrollmentRepository : IEnrollmentRepository
    {
        private readonly ApplicationDbContext dbContext;

        public EnrollmentRepository(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(int userId, int courseId)
        {
            Enrollment enrollment = new Enrollment
            {
                ApplicationUserId = userId,
                CourseId = courseId,
            };

            dbContext.Enrollment.Add(enrollment);
            dbContext.SaveChanges();
        }

        public void Delete(int userId, int courseId) ///??????
        {
            Enrollment current = dbContext.Enrollment.Where(e => e.ApplicationUserId == userId && e.CourseId == courseId).FirstOrDefault();
            if (current == null)
            {
                return;
            }
            dbContext.Enrollment.Remove(current);
            dbContext.SaveChanges();
        }


        public bool IsEnrolled(int userId, int courseId)
        {
            return dbContext.Enrollment.Any(e => e.CourseId == courseId && e.ApplicationUserId == userId);
        }
    }
}
