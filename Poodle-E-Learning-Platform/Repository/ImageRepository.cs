﻿using Poodle_e_Learning_Platform.Data;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository.Contracts;
using System.Linq;

namespace Poodle_e_Learning_Platform.Repository
{
    public class ImageRepository : IImageRepository
    {
        private readonly ApplicationDbContext dbContext;

        public ImageRepository(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(ImageData data)
        {
            dbContext.Images.Add(data);
            dbContext.SaveChanges();
        }

        public byte[] GetVisualize(string id)
        {
            return dbContext.Images.Where(i => i.Id == id).Select(i => i.VisualizeContent).FirstOrDefault();
        }

        public byte[] GetOriginal(string id)
        {
            return dbContext.Images.Where(i => i.Id == id).Select(i => i.OriginalContent).FirstOrDefault();
        }

        public byte[] GetThumbnail(string id)
        {
            return dbContext.Images.Where(i => i.Id == id).Select(i => i.ThumbnailContent).FirstOrDefault();
        }

        public int GetUploadNumber(int userId)
        {
            int number = dbContext
                .Users
                .Where(u => u.Id == userId)
                .OrderByDescending(u => u.Images.Select(i => i.UploadNumber).FirstOrDefault())
                .Select(u => u.Images.Select(i => i.UploadNumber).FirstOrDefault())
                .FirstOrDefault();

            return ++number;
        }

        public int GetCourseUploadNumber(int userId)
        {
            int number = dbContext
                .Users
                .Where(u => u.Id == userId)
                .OrderByDescending(u => u.CourseImages.Select(i => i.UploadNumber).FirstOrDefault())
                .Select(u => u.CourseImages.Select(i => i.UploadNumber).FirstOrDefault())
                .FirstOrDefault();

            return ++number;
        }

        public void CreateCourseImage(CourseImage image)
        {
            dbContext.CoursePictures.Add(image);
            dbContext.SaveChanges();
        }

        public byte[] GetCourseImageVisualize(string id)
        {
            byte[] content = dbContext
                .CoursePictures
                .Where(cp => cp.Id == id)
                .Select(cp => cp.VisualizeContent)
                .FirstOrDefault();

            return content;
        }
    }
}
