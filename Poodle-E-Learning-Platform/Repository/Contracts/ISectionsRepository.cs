﻿using Poodle_e_Learning_Platform.InputModels.Section;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.ViewModels.Sections;

namespace Poodle_e_Learning_Platform.Repository.Contracts
{
    public interface ISectionsRepository
    {
        void Create(SectionInputModel inputModel);

        int Delete(int id);

        EditSectionInputModel GetEditSectionData(int id);

        void Update(EditSectionInputModel inputModel);

        SectionDetailsViewModel GetSectionDetails(int id);

        int GetCourseIdBySectionId(int id);
        SectionPage GetSectionInfo(int id);
        public bool Exists(string title);
    }
}
