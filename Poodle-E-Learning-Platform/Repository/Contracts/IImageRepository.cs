﻿using Poodle_e_Learning_Platform.Models;

namespace Poodle_e_Learning_Platform.Repository.Contracts
{
    public interface IImageRepository
    {
        void Create(ImageData data);

        void CreateCourseImage(CourseImage image);

        byte[] GetOriginal(string id);

        byte[] GetVisualize(string id);

        byte[] GetThumbnail(string id);

        byte[] GetCourseImageVisualize(string id);

        int GetUploadNumber(int userId);

        int GetCourseUploadNumber(int userId);
    }
}
