﻿using Poodle_e_Learning_Platform.InputModels.ApplicationUser;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.ViewModels.ApplicationUsers;
using Poodle_e_Learning_Platform.ViewModels.Student;
using System.Collections.Generic;

namespace Poodle_e_Learning_Platform.Repository.Contracts
{
    public interface IApplicationUserRepository
    {
		IEnumerable<ApplicationUserViewModel> GetAllUsers();

		ApplicationUserViewModel GetUserDetails(int id);

		List<ApplicationUser> Get();

		ApplicationUser Get(int id);
		ApplicationUser Get(string email);
		bool Exists(string email);
		ApplicationUser Create(ApplicationUser teacher);

		bool IsTeacher(int id);

		IEnumerable<StudentViewModel> GetAllStudents(int courseId);

		ApplicationUserInputModel GetApplicationUserData(int id);

		void EditUser(ApplicationUserInputModel inputModel);

		void Delete(int id);
	}
}
