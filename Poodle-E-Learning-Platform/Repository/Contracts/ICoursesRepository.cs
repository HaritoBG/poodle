﻿using Poodle_e_Learning_Platform.InputModels.Course;
using Poodle_e_Learning_Platform.ViewModels.Course;
using Poodle_e_Learning_Platform.ViewModels.Home;
using Poodle_e_Learning_Platform.Models;
using System.Collections.Generic;

namespace Poodle_e_Learning_Platform.Repository.Contracts
{
    public interface ICoursesRepository
    {
        List<Course> Get();

        Course Get(int id);

        Course Get(string title);

        bool Exists(string title);  

        Course Create(CourseInputModel inputModel);

        Course Delete(int id);

        DetailsViewModel GetCourseDetails(int id);

        EnrollViewModel GetAllStudents(int courseId);

        IEnumerable<IndexItemViewModel> GetPublicCourses();

        IEnumerable<IndexItemViewModel> GetStudentPrivateCourses(int id);

        bool IsPrivate(int id);

        EditCourseInputModel GetEditCouse(int id);

        void Edit(EditCourseInputModel inputModel);
    }
}
