﻿namespace Poodle_e_Learning_Platform.Repository.Contracts
{
    public interface IEnrollmentRepository
    {
        bool IsEnrolled(int userId, int courseId);

        void Create(int userId, int courseId);

        void Delete(int userId, int courseId);//???
    }
}
