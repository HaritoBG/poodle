﻿using Poodle_e_Learning_Platform.Data;
using Poodle_e_Learning_Platform.InputModels.Section;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository.Contracts;
using Poodle_e_Learning_Platform.ViewModels.Sections;
using System;
using System.Linq;

namespace Poodle_e_Learning_Platform.Repository
{
    public class SectionsRepository : ISectionsRepository
    {
        private readonly ApplicationDbContext dbContext;

        public SectionsRepository(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(SectionInputModel inputModel)
        {
            SectionPage sectionPage = new SectionPage
            {
                Content = inputModel.Content,
                CourseId = inputModel.CourseId,
                dateTime = DateTime.Now,
                Order = 0,
                Title = inputModel.Title,
            };
            dbContext.SectionPages.Add(sectionPage);
            dbContext.SaveChanges();
        }

        public int Delete(int id)
        {
            SectionPage current = dbContext.SectionPages.Where(sp => sp.Id == id).FirstOrDefault();
            if (current is null)
            {
                return 0;
            }
            int courseId = current.CourseId;
            dbContext.SectionPages.Remove(current);
            dbContext.SaveChanges();
            return courseId;
        }

        public bool Exists(string title)
        {
            return dbContext.SectionPages.Where(sp => sp.Title == title).Any();
        }

        public int GetCourseIdBySectionId(int id)
        {
            int courseId = dbContext
                .SectionPages
                .Where(cp => cp.Id == id)
                .Select(cp => cp.CourseId)
                .FirstOrDefault();

            return courseId;
        }

        public EditSectionInputModel GetEditSectionData(int id)
        {
            EditSectionInputModel model = dbContext
                .SectionPages
                .Where(s => s.Id == id)
                .Select(s => new EditSectionInputModel
                {
                    CourseId = s.CourseId,
                    CourseTitle = s.Course.Title,
                    SectionId = id,
                    Content = s.Content,
                    Title = s.Title,
                    Order = s.Order,
                })
                .FirstOrDefault();


            return model;
        }

        public SectionDetailsViewModel GetSectionDetails(int id)
        {
            SectionDetailsViewModel viewModel = dbContext
                .SectionPages
                .Where(sp => sp.Id == id)
                .Select(sp => new SectionDetailsViewModel
                {
                    Id = sp.Id,
                    Title = sp.Title,
                    Content = sp.Content,
                    CourseId = sp.CourseId,
                })
                .FirstOrDefault();

            return viewModel;
        }

        public SectionPage GetSectionInfo(int id)
        {
            return dbContext.SectionPages.Where(sp=>sp.Id == id).FirstOrDefault();
        }

        public void Update(EditSectionInputModel inputModel)
        {
            SectionPage section = dbContext
                .SectionPages
                .FirstOrDefault();

            if (section is null)
            {
                return;
            }

            section.Title = inputModel.Title;
            section.Order = inputModel.Order;
            section.Content = inputModel.Content;

            dbContext.SaveChanges();

        }
    }
}
