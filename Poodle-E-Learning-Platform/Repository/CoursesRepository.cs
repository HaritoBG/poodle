﻿
using Poodle_e_Learning_Platform.Data;
using Poodle_e_Learning_Platform.Exceptions;
using Poodle_e_Learning_Platform.InputModels.Course;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository.Contracts;
using Poodle_e_Learning_Platform.ViewModels.Course;
using Poodle_e_Learning_Platform.ViewModels.Home;
using Poodle_e_Learning_Platform.ViewModels.Sections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Poodle_e_Learning_Platform.Repository
{
    public class CoursesRepository : ICoursesRepository
    {
        private readonly ApplicationDbContext context;
        private readonly IApplicationUserRepository applicationUserRepository;

        public CoursesRepository(ApplicationDbContext context, IApplicationUserRepository applicationUserRepository)
        {
            this.context = context;
            this.applicationUserRepository = applicationUserRepository;
        }

        public List<Course> Get()
        {
            return this.GetCourses().ToList();
        }

        public Course Get(int id)
        {
            return this.GetCourses().Where(t => t.Id == id).FirstOrDefault() ?? throw new EntityNotFoundException();
        }

        public Course Get(string title)
        {
            return this.GetCourses().Where(t => t.Title == title).FirstOrDefault() ?? throw new EntityNotFoundException();
        }

        public bool Exists(string title)
        {
            return this.context.Courses.Any(u => u.Title == title);
        }
        public Course Create(CourseInputModel inputModel)
        {
            Course course = new Course
            {
                Title = inputModel.Title,
                Description = inputModel.Description,
                ApplicationUserId = inputModel.ApplicationUserId,
                IsPrivate = inputModel.IsPrivate,
                StartDate = inputModel.StartDate,
                EndDate = inputModel.EndDate,
                Created = DateTime.Now,
            };

            context.Courses.Add(course);
            context.SaveChanges();
            return course;
        }

        public Course Delete(int id)
        {

            SectionPage[] courseSections = context
                .SectionPages
                .Where(sp => sp.CourseId == id)
                .ToArray();

            context
                .SectionPages
                .RemoveRange(courseSections);

                .Where(c => c.Id == id)
                .FirstOrDefault();

            context.Courses.Remove(course);
            context.SaveChanges();
            return course;

        }

        private IQueryable<Course> GetCourses()//???
        {
            return context.Courses;            
            
        }

        public DetailsViewModel GetCourseDetails(int id)
        {
            DetailsViewModel model = context
                .Courses
                .Where(c => c.Id == id)
                .Select(c => new DetailsViewModel
                {
                    Id = c.Id,
                    Title = c.Title,
                    Description = c.Description,
                    EndDate = c.EndDate,
                    StartDate = c.StartDate,
                    IsPrivate = c.IsPrivate,
                    ImageId = c.Images.OrderByDescending(i => i.UploadNumber).Select(i => i.Id).FirstOrDefault(),
                    Sections = c.Sections
                        .Where(s => s.CourseId == id)
                        .OrderBy(c => c.Order)
                        .Select(s => new SectionViewModel
                        {
                            Content = s.Content,
                            Title = s.Title,
                            Id = s.Id,
                        }).ToList()
                })
                .FirstOrDefault();
            
            return model;
        }

        public EnrollViewModel GetAllStudents(int courseId)
        {
            EnrollViewModel viewModel = context
                .Courses
                .Where(c => c.Id == courseId)
                .Select(c => new EnrollViewModel
                {
                    CourseId = c.Id,
                    CourseName = c.Title,
                })
                .FirstOrDefault();
            viewModel.Students = applicationUserRepository.GetAllStudents(courseId);
            return viewModel;
        }

        public IEnumerable<IndexItemViewModel> GetPublicCourses()
        {
            IEnumerable<IndexItemViewModel> model = context
                .Courses
                .Where(c => !c.IsPrivate && c.EndDate >= DateTime.Now)
                .Select(c => new IndexItemViewModel
                {
                    Id = c.Id,
                    Title = c.Title,
                    Content = c.Description,
                })
                .ToList();

            return model;
        }

        public IEnumerable<IndexItemViewModel> GetStudentPrivateCourses(int id)
        {
            IEnumerable<IndexItemViewModel> courses = context
                .Courses
                .Where(c => c.IsPrivate && c.Enrollments.Any(e => e.ApplicationUserId == id) && c.EndDate >= DateTime.Now)
                .Select(c => new IndexItemViewModel
                {
                    Id = c.Id,
                    Title = c.Title,
                    Content = c.Description,
                })
                .ToList();

            return courses;
        }

        public bool IsPrivate(int id)
        {
            return context.Courses.Where(c => c.Id == id).Select(c => c.IsPrivate).FirstOrDefault();
        }

        public EditCourseInputModel GetEditCouse(int id)
        {
            EditCourseInputModel model = context.Courses
                .Where(c => c.Id == id)
                .Select(c => new EditCourseInputModel
                {
                    Title = c.Title,
                    StartDate = c.StartDate,
                    IsPrivate = c.IsPrivate,
                    EndDate = c.EndDate,
                    ApplicationUserId = c.ApplicationUserId,
                    Description = c.Description,
                    CourseId = c.Id,
                })
                .FirstOrDefault();



            return model;
        }

        public void Edit(EditCourseInputModel inputModel)
        {
            Course course = context
                .Courses
                .Where(c => c.Id == inputModel.CourseId)
                .FirstOrDefault();

            course.Title = inputModel.Title;
            course.Description = inputModel.Description;
            course.StartDate = inputModel.StartDate;
            course.EndDate = inputModel.EndDate;
            course.IsPrivate = inputModel.IsPrivate;

            context.SaveChanges();
        }
    }
}
