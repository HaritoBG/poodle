﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Poodle_e_Learning_Platform.Data;
using Poodle_e_Learning_Platform.Exceptions;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository.Contracts;
using Poodle_e_Learning_Platform.ViewModels.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using Poodle_e_Learning_Platform.Globals;
using Poodle_e_Learning_Platform.InputModels.ApplicationUser;
using Poodle_e_Learning_Platform.Services.Contracts;
using Poodle_e_Learning_Platform.InputModels.Images;
using Microsoft.AspNetCore.Identity;
using Poodle_e_Learning_Platform.ViewModels.ApplicationUsers;

namespace Poodle_e_Learning_Platform.Repository
{
    public class ApplicationUserRepository : IApplicationUserRepository
    {

        private readonly ApplicationDbContext context;

        {
            this.context = context;
        }


        public List<ApplicationUser> Get()
        {
            return this.GetTeachers().ToList();
        }

        public ApplicationUser Get(int id)
        {
            return this.GetTeachers().Where(t => t.Id == id).FirstOrDefault() ?? throw new EntityNotFoundException();
        }

        public ApplicationUser Get(string email)
        {
            return this.GetTeachers().Where(t => t.Email == email).FirstOrDefault() ?? throw new EntityNotFoundException();
        }

        public bool Exists(string email)
        {

            return this.context.Users.Any(u => u.Email == email);
        }
        public ApplicationUser Create(ApplicationUser teacher)
        {

            EntityEntry<ApplicationUser> createdTeacher = this.context.Users.Add(teacher);
            this.context.SaveChanges();
            return createdTeacher.Entity;
        }

        private IQueryable<ApplicationUser> GetTeachers()
        {
            
            return this.context.Users.Include(t => t.Courses);
        }

        public ApplicationUser Update(int id, ApplicationUser teacher)
        {
            ApplicationUser existingTeacher = this.Get(id);
            existingTeacher.Email = teacher.Email;
            existingTeacher.FirstName = teacher.FirstName;
            existingTeacher.LastName = teacher.LastName;
            existingTeacher.ProfilePicture = teacher.ProfilePicture;// change picture ?
            
            existingTeacher.PasswordHash = teacher.PasswordHash;
            this.context.SaveChanges();
            return existingTeacher;
        }

        public bool IsTeacher(int id)
        {
            return context.UserRoles.Any(ur => ur.UserId == id && ur.RoleId == GlobalConstants.TEACHER_ROLE_ID);
        }

        public IEnumerable<StudentViewModel> GetAllStudents(int courseId)
        {
            var result = context.UserRoles.ToList();

            int roleId = context.UserRoles.Where(ur => ur.UserId == 1).Select(ur => ur.RoleId).FirstOrDefault();

            IEnumerable<StudentViewModel> students = context
                .Users
                .Select(u => new StudentViewModel
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Email = u.Email,
                    IsEnrolled = u.Enrollments.Any(e => e.ApplicationUserId == u.Id && e.CourseId == courseId),
                    RoleId = context.UserRoles.Where(ur => ur.UserId == u.Id).Select(ur => ur.RoleId).FirstOrDefault()
                })
                .ToList();

            IEnumerable<StudentViewModel> resultStudents = students.Where(s => s.RoleId == GlobalConstants.STUDENT_ROLE_ID).ToList();

            return resultStudents;
        }

        public ApplicationUserInputModel GetApplicationUserData(int id)
        {
            ApplicationUserInputModel model = context
                .Users
                .Where(u => u.Id == id)
                .Select(u => new ApplicationUserInputModel
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Email = u.Email,
                })
                .FirstOrDefault();

            return model;
        }

        public void EditUser(ApplicationUserInputModel inputModel)
        {
            ApplicationUser user = context.Users.Where(u => u.Id == inputModel.Id).FirstOrDefault();

            string imagePath = string.Empty;



            user.Email = inputModel.Email;
            user.UserName = inputModel.Email;
            user.FirstName = inputModel.FirstName;
            user.LastName = inputModel.LastName;

            if (!string.IsNullOrEmpty(imagePath))
            {
                user.ProfilePicture = imagePath;
            }

            context.SaveChanges();

        }

        public void Delete(int id)
        {
            ApplicationUser user = context
                .Users
                .Where(u => u.Id == id)
                .FirstOrDefault();

            IEnumerable<IdentityUserRole<int>> userRoles = context
                .UserRoles
                .Where(ur => ur.UserId == id)
                .ToList();

            IEnumerable<Enrollment> userEnrollments = context
                .Enrollment
                .Where(e => e.ApplicationUserId == id)
                .ToList();



            context.UserRoles.RemoveRange(userRoles);
            context.Enrollment.RemoveRange(userEnrollments);
            context.Users.Remove(user);
            context.SaveChanges();
        }

        public IEnumerable<ApplicationUserViewModel> GetAllUsers()
        {
            IEnumerable<ApplicationUserViewModel> model = context
                .Users
                .Select(u => new ApplicationUserViewModel
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Email = u.Email,
                    Username = u.UserName,
                })
                .ToList();

            return model;
        }

        public ApplicationUserViewModel GetUserDetails(int id)
        {
            var user = context
                .Users
                .Where(u => u.Id == id)
                .Select(u => new ApplicationUserViewModel
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Email = u.Email,
                    Username = u.UserName,
                })
                .FirstOrDefault();

            return user;
        }
    }
}
