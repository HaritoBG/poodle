﻿using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Poodle_e_Learning_Platform.Services.Contracts;
using Poodle_e_Learning_Platform.Models;


namespace Poodle_e_Learning_Platform.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICoursesService coursesService;

        public HomeController(ILogger<HomeController> logger, ICoursesService coursesService)
        {
            _logger = logger;
            this.coursesService = coursesService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
