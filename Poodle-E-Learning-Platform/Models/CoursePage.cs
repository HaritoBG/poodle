﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Poodle_e_Learning_Platform.Models
{
    public class CoursePage///not needed??
    {
        
        [Required]
        public string Title { get; set; } 

        [Required]
        public string Description { get; set; }

        public string Section { get; set; }

        public virtual IEnumerable<SectionPage> Sections { get; set; } = new HashSet<SectionPage>();
    }
}
