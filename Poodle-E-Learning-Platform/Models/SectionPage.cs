﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Poodle_e_Learning_Platform.Models
{
    public class SectionPage
    {        
        public int Id { get; set; }

        [Required]
        public string Title { get; set; } // UNIQUE

        [Required]
        public string Content { get; set; } // HTML

        public int Order { get; set; }

        public int CourseId { get; set; }

        public virtual Course Course { get; set; } 
        
        public DateTime dateTime { get; set; } // restricted by date or for specific users or no restrictions??
    }
}
