﻿namespace Poodle_e_Learning_Platform.Models
{
    public class Enrollment
    {
        public int ApplicationUserId { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public int CourseId { get; set; }

        public virtual Course Course { get; set; }

    }
}
