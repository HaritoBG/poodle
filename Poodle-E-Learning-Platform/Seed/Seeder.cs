﻿using Microsoft.AspNetCore.Identity;
using Poodle_e_Learning_Platform.Data;
using Poodle_e_Learning_Platform.Globals;
using Poodle_e_Learning_Platform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poodle_e_Learning_Platform.Seed
{
    public static class Seeder
    {
        const string AdminUserName = "admin@abv.bg";
        const string AdminRoleName = "Admin";
        const string TeacherUserName = "Teacher@abv.bg";
        const string TeacherRoleName = "Teacher";

        public static void SeedData(UserManager<ApplicationUser> userManager, ApplicationDbContext dbContext)
        {

            Task<ApplicationUser> currentUser = userManager.FindByEmailAsync(AdminUserName);

            if (!(currentUser.Result is null))
            {
                return;
            }

            ApplicationUser adminUser = new ApplicationUser
            {
                Email = "admin@abv.bg",
                UserName = AdminUserName,
                FirstName = "Admin",
                LastName = "Admin",
                
            };

            ApplicationUser teacherUser = new ApplicationUser
            {
                Email = TeacherUserName,
                UserName = TeacherUserName,
                FirstName = "Ivan",
                LastName = "Ivanov",
            };


            IdentityResult newAdmin = userManager.CreateAsync(adminUser, "admin").Result;
            IdentityResult newTeacher = userManager.CreateAsync(teacherUser, "teacher").Result;
            List<IdentityRole<int>> roles = new List<IdentityRole<int>>();

            roles.Add(new IdentityRole<int> 
            {   Name = AdminRoleName,
                NormalizedName = AdminRoleName.ToUpper(), 
                ConcurrencyStamp = Guid.NewGuid().ToString() });
            roles.Add(new IdentityRole<int>
            {   Name = TeacherRoleName,
                NormalizedName = TeacherRoleName.ToUpper(), 
                ConcurrencyStamp = Guid.NewGuid().ToString() });
            roles.Add(new IdentityRole<int> 
            {   Name = "Student", 
                NormalizedName = "Student".ToUpper(), 
                ConcurrencyStamp = Guid.NewGuid().ToString() 
            });


            dbContext.Roles.AddRange(roles);
            dbContext.SaveChanges();

            AddAdminRole(dbContext);
        }

        public static void AddAdminRole(ApplicationDbContext dbContext) 
        {
            int adminUserId = dbContext.Users.Where(u => u.UserName == AdminUserName).Select(u => u.Id).FirstOrDefault();
            int adminRoleId = dbContext.Roles.Where(u => u.Name == AdminRoleName).Select(r => r.Id).FirstOrDefault();

            IdentityUserRole<int> adminRole = new IdentityUserRole<int>
            {
                RoleId = adminRoleId,
                UserId = adminUserId,
            };

            int teacherUserId = dbContext.Users.Where(u => u.UserName == TeacherUserName).Select(u => u.Id).FirstOrDefault();
            int teacherRoleId = dbContext.Roles.Where(u => u.Name == TeacherRoleName).Select(r => r.Id).FirstOrDefault();

            IdentityUserRole<int> teacherRole = new IdentityUserRole<int>
            {
                RoleId = teacherRoleId,
                UserId = teacherUserId,
            };

            dbContext.UserRoles.Add(adminRole);
            dbContext.UserRoles.Add(teacherRole);
            dbContext.SaveChanges();

        }
    }
}
