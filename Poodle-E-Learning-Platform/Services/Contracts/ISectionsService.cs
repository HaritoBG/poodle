﻿using Poodle_e_Learning_Platform.InputModels.Section;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.ViewModels.Sections;

namespace Poodle_e_Learning_Platform.Services.Contracts
{
    public interface ISectionsService
    {
        void Create(SectionInputModel inputModel);


        EditSectionInputModel GetEditSectionData(int id);



        public bool Exists(string title);
    }
}
