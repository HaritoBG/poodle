﻿using Poodle_e_Learning_Platform.InputModels.Course;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.ViewModels.Course;
using Poodle_e_Learning_Platform.ViewModels.Home;
using System.Collections.Generic;

namespace Poodle_e_Learning_Platform.Services.Contracts
{
    public interface ICoursesService
    {
        List<Course> Get();
        Course Get(int id);
        Course Get(string firstName);
        bool Exists(string email);
        Course Create(CourseInputModel inputModel);

        void Delete(int id);

        DetailsViewModel GetCourseDetails(int id);

        EnrollViewModel GetAllStudents(int courseId);


        IEnumerable<IndexItemViewModel> GetStudentPrivateCourses(int id);


    }
}
