﻿using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.ViewModels.ApplicationUsers;
using Poodle_e_Learning_Platform.ViewModels.Student;
using System.Collections.Generic;

namespace Poodle_e_Learning_Platform.Services.Contracts
{
    public interface IApplicationUserService
    {
        IEnumerable<ApplicationUserViewModel> GetAllUsers();

        ApplicationUserViewModel GetUserDetails(int id);

        List<ApplicationUser> Get();
        ApplicationUser Get(int id);
        ApplicationUser Get(string firstName);
        bool Exists(string email);
        ApplicationUser Create(ApplicationUser teacher);

        public bool IsTeacher(int id);

        public bool IsEnrolled(int courseId, int userId);

        IEnumerable<StudentViewModel> GetAllStudents(int courseId);

        ApplicationUserInputModel ApplicationUserData(int id);

        void EditUser(ApplicationUserInputModel inputModel);

        void Delete(int id);

    }
}
