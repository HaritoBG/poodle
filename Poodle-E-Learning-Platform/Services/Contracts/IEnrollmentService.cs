﻿namespace Poodle_e_Learning_Platform.Services.Contracts
{
    public interface IEnrollmentService
    {
        bool IsEnrolled(int userId, int courseId);

        void Create(int userId, int courseId);

        void Delete(int userId, int courseId);

        public void DeleteAll(int courseId);


    }
}
