﻿using Poodle_e_Learning_Platform.Dto.Videos;
using System.Collections.Generic;

namespace Poodle_e_Learning_Platform.Services.Contracts
{
    public interface IVideoServise
    {
        IEnumerable<VideoDto> GetAllVideos();
    }
}
