﻿using Microsoft.AspNetCore.Http;
using Poodle_e_Learning_Platform.InputModels.Images;
using System.IO;

namespace Poodle_e_Learning_Platform.Services.Contracts
{
    public interface IImageService
    {
        void SaveImage(ImageInputModel image);

        void SaveCourseImage(CourseImageInputModel inputModel);

        byte[] GetOrignal(string id);

        byte[] GetVisualize(string id);

        byte[] GetThumbnail(string id);

        byte[] GetCourseImageVisualize(string id);
    }
}
