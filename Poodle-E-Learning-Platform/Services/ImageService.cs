﻿using Microsoft.AspNetCore.Http;
using Poodle_e_Learning_Platform.InputModels.Images;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository.Contracts;
using Poodle_e_Learning_Platform.Services.Contracts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Processing;
using System;
using System.IO;
using System.Reflection;

namespace Poodle_e_Learning_Platform.Services
{
    public class ImageService : IImageService
    {
        private const int FullscreenWidth = 1000;
        private const int ThumbnailWidht = 300;


        private const string ROOT_FOLDER_NAME = "wwwroot";
        private const string IMAGE_FOLDER_NAME = "Pictures";

        private readonly string path;
        private readonly string fullPath;
        private readonly IImageRepository imageRepository;

        
        public ImageService(IImageRepository imageRepository)
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            fullPath = Path.Combine(path, ROOT_FOLDER_NAME, IMAGE_FOLDER_NAME);
            this.imageRepository = imageRepository;
        }

        public byte[] GetCourseImageVisualize(string id)
        {
            return imageRepository.GetCourseImageVisualize(id);
        }

        public byte[] GetOrignal(string id)
        {
            return imageRepository.GetOriginal(id);
        }

        public byte[] GetThumbnail(string id)
        {
            return imageRepository.GetThumbnail(id);
        }

        public byte[] GetVisualize(string id)
        {
            return imageRepository.GetVisualize(id);
        }

        public void SaveCourseImage(CourseImageInputModel inputModel)
        {
            Image imageFile = Image.Load(inputModel.ImageData);
            byte[] origalContent = GenImageBytes(imageFile, imageFile.Width);
            byte[] visualizeContent = GenImageBytes(imageFile, ThumbnailWidht);
            CourseImage courseImage = new CourseImage
            {
                VisualizeContent = visualizeContent,
                OriginalContent = origalContent,
                CourseId = inputModel.CourseId,
                Id = Guid.NewGuid().ToString(),
                OriginalName = inputModel.OriginalName,
                OriginalType = inputModel.OriginalType,
                UserId = inputModel.UserId,
                UploadNumber = imageRepository.GetCourseUploadNumber(inputModel.UserId),
            };

            imageRepository.CreateCourseImage(courseImage);
        }

        public void SaveImage(ImageInputModel inputModel)
        {
            //From ImageSharp Nuget Package
            Image imageFile = Image.Load(inputModel.ImageData);
            byte[] originalContent = GenImageBytes(imageFile, imageFile.Width);
            byte[] visualizeContent = GenImageBytes(imageFile, FullscreenWidth);
            byte[] thumbnailContent = GenImageBytes(imageFile, ThumbnailWidht);

            int uploadNumber = imageRepository.GetUploadNumber(inputModel.UserId);

            ImageData data = new ImageData
            {
                Id = Guid.NewGuid().ToString(),
                UserId = inputModel.UserId,
                OriginalName = inputModel.OriginalName,
                OriginalType = inputModel.OriginalType,
                OriginalContent = originalContent,
                VisualizeContent = visualizeContent,
                ThumbnailContent = thumbnailContent,
                UploadNumber = uploadNumber,
            };
            imageRepository.Create(data);

        }

        private byte[] GenImageBytes(Image image, int size) 
        {
            int width = image.Width;
            int height = image.Height;

            if (width > size)
            {
                height = (int)((double)size / width * height);
                width = size;
            }

            image.Mutate(i => i.Resize(new Size(width, height)));

            using MemoryStream resultStream = new MemoryStream();

            image.SaveAsJpeg(resultStream, new JpegEncoder
            {
                Quality = 75,
            });

            return resultStream.ToArray();
        }

    }
}
