﻿using Newtonsoft.Json;
using Poodle_e_Learning_Platform.Dto.Videos;
using Poodle_e_Learning_Platform.Services.Contracts;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Poodle_e_Learning_Platform.Services
{
    public class VideoService : IVideoServise
    {
        private const string WWWROOT_FOLDER_NAME = "wwwroot";
        private const string VIDEO_PATH_FOLDER_NAME = "VideoPath";
        private const string TEXT_FILE_NAME = "videos.txt";
        private readonly string currentDirectory;
        private readonly string fullPath;

        public VideoService()
        {
            currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            fullPath = Path.Combine(currentDirectory, WWWROOT_FOLDER_NAME, VIDEO_PATH_FOLDER_NAME, TEXT_FILE_NAME); 

        }

        public IEnumerable<VideoDto> GetAllVideos()
        {
            string inputJson = File.ReadAllText(fullPath);

            IEnumerable<VideoDto> result = JsonConvert.DeserializeObject<IEnumerable<VideoDto>>(inputJson);
            JsonConvert.SerializeObject(result);

            return result;
        }
    }
}
