﻿using Poodle_e_Learning_Platform.Exceptions;
using Poodle_e_Learning_Platform.Globals;
using Poodle_e_Learning_Platform.InputModels.Course;
using Poodle_e_Learning_Platform.InputModels.Images;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository.Contracts;
using Poodle_e_Learning_Platform.Services.Contracts;
using Poodle_e_Learning_Platform.ViewModels.Course;
using Poodle_e_Learning_Platform.ViewModels.Home;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Poodle_e_Learning_Platform.Services
{
    public class CoursesService : ICoursesService
    {
        private readonly ICoursesRepository repository;
        private readonly IImageService imageService;

        public CoursesService(ICoursesRepository repository, IImageService imageService)
        {
            this.repository = repository;
            this.imageService = imageService;
        }

        public List<Course> CourseGetter(int id)
        {
            return repository.CourseGetter(id);
        }
        public List<Course> Get()
        {
            return this.repository.Get();
        }

        public Course Get(int id)
        {
            return this.repository.Get(id);
        }

        public Course Get(string title)
        {
            return this.repository.Get(title);
        }

        public bool Exists(string title)
        {
            return this.repository.Exists(title);
        }

        public Course Create(CourseInputModel inputModel)
        {
            return repository.Create(inputModel);
        }

        public void Delete(int id)
        {
            this.repository.Delete(id);
        }

        public DetailsViewModel GetCourseDetails(int id)
        {
            DetailsViewModel model = repository.GetCourseDetails(id);

            return model;

        }

        public EnrollViewModel GetAllStudents(int courseId)
        {
            EnrollViewModel model =repository.GetAllStudents(courseId);

            return model;
        }

        {
        }

        {
        }
    }
}
