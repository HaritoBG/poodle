﻿using Poodle_e_Learning_Platform.Repository.Contracts;
using Poodle_e_Learning_Platform.Services.Contracts;

namespace Poodle_e_Learning_Platform.Services
{
    public class EnrollmentService : IEnrollmentService
    {
        private readonly IEnrollmentRepository enrollmentRepository;

        public EnrollmentService(IEnrollmentRepository enrollmentRepository)
        {
            this.enrollmentRepository = enrollmentRepository;
        }

        public void Create(int userId, int courseId)
        {
            enrollmentRepository.Create(userId, courseId);
        }

        public bool IsEnrolled(int userId, int courseId)
        {
            return enrollmentRepository.IsEnrolled(userId, courseId);
        }

        public void Delete(int userId, int courseId)
        {
            enrollmentRepository.Delete(userId, courseId);
        }

        public void DeleteAll(int courseId)
        {
            enrollmentRepository.DeleteAll(courseId);
        }
    }
}
