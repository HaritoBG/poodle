﻿using Poodle_e_Learning_Platform.InputModels.ApplicationUser;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository.Contracts;
using Poodle_e_Learning_Platform.Services.Contracts;
using Poodle_e_Learning_Platform.ViewModels.ApplicationUsers;
using Poodle_e_Learning_Platform.ViewModels.Student;
using System.Collections.Generic;
using System.Linq;

namespace Poodle_e_Learning_Platform.Services
{
    public class ApplicationUserService : IApplicationUserService
    {

        private readonly IApplicationUserRepository repository;
        private readonly ICoursesRepository coursesRepository; //???

        {
            this.repository = repository;
        }
        public List<ApplicationUser> Get()
        {
            return this.repository.Get();
        }
        public ApplicationUser Get(int id)
        {
            return this.repository.Get(id);
        }

        public ApplicationUser Get(string email)
        {
            return this.repository.Get(email);
        }
        public bool Exists(string email)
        {
            return this.repository.Exists(email);
        }
        public ApplicationUser Create(ApplicationUser teacher)
        {
            return this.repository.Create(teacher);
        }

        public bool IsTeacher(int id) 
        {
            return repository.IsTeacher(id);
        }

        public bool IsEnrolled(int courseId, int userId)
        {
            return enrollmentRepository.IsEnrolled(courseId, userId);
        }
        public IEnumerable<StudentViewModel> GetAllStudents(int courseId)
        {
            IEnumerable<StudentViewModel> students = repository.GetAllStudents(courseId);

            return students;
        }

        public ApplicationUserInputModel ApplicationUserData(int id)
        {
            ApplicationUserInputModel model = repository.GetApplicationUserData(id);
            return model;
        }

        public void EditUser(ApplicationUserInputModel inputModel)
        {
            repository.EditUser(inputModel);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
        }

        public IEnumerable<ApplicationUserViewModel> GetAllUsers()
        {
            return repository.GetAllUsers();
        }

        public ApplicationUserViewModel GetUserDetails(int id)
        {
            return repository.GetUserDetails(id);
        }
    }
}
