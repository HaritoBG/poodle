﻿using Poodle_e_Learning_Platform.InputModels.Section;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository.Contracts;
using Poodle_e_Learning_Platform.Services.Contracts;
using Poodle_e_Learning_Platform.ViewModels.Sections;

namespace Poodle_e_Learning_Platform.Services
{
    public class SectionsService : ISectionsService
    {
        private readonly ISectionsRepository sectionsRepository;

        public SectionsService(ISectionsRepository sectionsRepository)
        {
            this.sectionsRepository = sectionsRepository;
        }

        public void Create(SectionInputModel inputModel)
        {
            sectionsRepository.Create(inputModel);
        }

        public int Delete(int id)
        {
           return sectionsRepository.Delete(id);
            
        }

        public bool Exists(string title)
        {
            return sectionsRepository.Exists(title);
        }

        public int GetCourseIdBySectionId(int id)
        {
            return sectionsRepository.GetCourseIdBySectionId(id);
        }

        public EditSectionInputModel GetEditSectionData(int id)
        {
            return sectionsRepository.GetEditSectionData(id);
        }

        public SectionDetailsViewModel GetSectionDetails(int id)
        {
            return sectionsRepository.GetSectionDetails(id);
        }

        public SectionPage GetSectionInfo(int id)
        {
            return sectionsRepository.GetSectionInfo(id);
        }

        public void Update(EditSectionInputModel inputModel)
        {
            sectionsRepository.Update(inputModel);
        }
    }
}
