﻿using Microsoft.AspNetCore.Identity;
using Moq;
using NUnit.Framework;
using Poodle_e_Learning_Platform.Controllers;
using Poodle_e_Learning_Platform.Data;
using Poodle_e_Learning_Platform.Exceptions;
using Poodle_e_Learning_Platform.InputModels.Course;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository;
using Poodle_e_Learning_Platform.Repository.Contracts;
using Poodle_e_Learning_Platform.Services;
using Poodle_e_Learning_Platform.Services.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poodle_e_Learning_Platform.Tests.CoursesService
{
    public class CreateCourse_Should
    {
        private Mock<ICoursesRepository> repositoryMock;
        private Mock<IImageService> imageServiceMock;
        private ICoursesService sut;

        [SetUp]
        public void Setup()
        {

            repositoryMock = new Mock<ICoursesRepository>();
            imageServiceMock = new Mock<IImageService>();
            sut = new Services.CoursesService(repositoryMock.Object, imageServiceMock.Object);
        }

        [Test]
        public void CreateCourse_When_ParametersAreValid()
        {
          
            CourseInputModel model = TestHelper.GetCourseInputModel(); 
            sut.Create(model);
            repositoryMock.Verify(rm => rm.Create(It.IsAny<CourseInputModel>()), Times.Once);

        }

        [Test]
        public void DeleteCourseTest()
        {
            sut.Delete(10);
            repositoryMock.Verify(rm => rm.Delete(It.IsAny<int>()), Times.Once);
        }

        [Test]
        public void GetCoursesTest() 
        {
            sut.Get();
            repositoryMock.Verify(rm => rm.Get(), Times.Once);
        }


    }
}
