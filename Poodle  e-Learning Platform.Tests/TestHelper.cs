﻿using Poodle_e_Learning_Platform.InputModels.Course;
using Poodle_e_Learning_Platform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poodle_e_Learning_Platform.Tests
{
    public class TestHelper
    {
        

        public static CourseInputModel GetCourseInputModel() 
        {
            return new CourseInputModel
            {
                ApplicationUserId = 1,
                Description = "Descrtiption",
                EndDate = DateTime.Now.AddDays(10),
                StartDate = DateTime.Now.AddDays(-5),
                IsPrivate = false,
                Title = "Title",
            };
        }

    }
}
