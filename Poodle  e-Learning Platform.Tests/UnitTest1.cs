using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using Poodle_e_Learning_Platform.Data;
using Poodle_e_Learning_Platform.InputModels.Course;
using Poodle_e_Learning_Platform.Models;
using Poodle_e_Learning_Platform.Repository;
using Poodle_e_Learning_Platform.Repository.Contracts;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Poodle_e_Learning_Platform.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
           
        }

        [Test]
        public void TestCreateCourse()
        {

            var mockSet = new Mock<DbSet<Course>>();
            var dbContext = new Mock<ApplicationDbContext>();
            dbContext.Setup(bc => bc.Courses).Returns(mockSet.Object);

            var appUserMockRep = new Mock<IApplicationUserRepository>();

            ICoursesRepository rep = new CoursesRepository(dbContext.Object, appUserMockRep.Object);

            CourseInputModel inputModel = new CourseInputModel
            {
                ApplicationUserId = 1,
                Description = string.Empty,
                EndDate = DateTime.Now.AddDays(3),
                IsPrivate = true,
                StartDate = DateTime.Now,
                Title = "Title",
            };

            rep.Create(inputModel);
            mockSet.Verify(m => m.Add(It.IsAny<Course>()), Times.Once());
            dbContext.Verify(dc => dc.SaveChanges(), Times.Once());
        }

        [Test]
        public void TestDeleteCourse()
        {
            var courses = new List<Course>()
            {
                new Course
                {
                    Id = 1,
                }
            }.AsQueryable();

            var sectionPages = new List<SectionPage>()
            {
                new SectionPage
                {
                    Id = 1,
                }
            }.AsQueryable();
            var mockSet = new Mock<DbSet<Course>>();

            var imageData = new List<CourseImage>()
            {
                new CourseImage
                {
                    Id = "1",
                }
            }.AsQueryable();

            mockSet.As<IQueryable<Course>>().Setup(m => m.Provider).Returns(courses.Provider);
            mockSet.As<IQueryable<Course>>().Setup(m => m.Expression).Returns(courses.Expression);
            mockSet.As<IQueryable<Course>>().Setup(m => m.ElementType).Returns(courses.ElementType);
            mockSet.As<IQueryable<Course>>().Setup(m => m.GetEnumerator()).Returns(courses.GetEnumerator());

            var sectionPageMockSet = new Mock<DbSet<SectionPage>>();

            sectionPageMockSet.As<IQueryable<SectionPage>>().Setup(m => m.Provider).Returns(sectionPages.Provider);
            sectionPageMockSet.As<IQueryable<SectionPage>>().Setup(m => m.Expression).Returns(sectionPages.Expression);
            sectionPageMockSet.As<IQueryable<SectionPage>>().Setup(m => m.ElementType).Returns(sectionPages.ElementType);
            sectionPageMockSet.As<IQueryable<SectionPage>>().Setup(m => m.GetEnumerator()).Returns(sectionPages.GetEnumerator());

            var imageDataMockSet = new Mock<DbSet<CourseImage>>();

            imageDataMockSet.As<IQueryable<CourseImage>>().Setup(m => m.Provider).Returns(imageData.Provider);
            imageDataMockSet.As<IQueryable<CourseImage>>().Setup(m => m.Expression).Returns(imageData.Expression);
            imageDataMockSet.As<IQueryable<CourseImage>>().Setup(m => m.ElementType).Returns(imageData.ElementType);
            imageDataMockSet.As<IQueryable<CourseImage>>().Setup(m => m.GetEnumerator()).Returns(imageData.GetEnumerator());

            var dbContext = new Mock<ApplicationDbContext>();
            dbContext.Setup(bc => bc.Courses).Returns(mockSet.Object);
            dbContext.Setup(dc => dc.SectionPages).Returns(sectionPageMockSet.Object);
            dbContext.Setup(dc => dc.CoursePictures).Returns(imageDataMockSet.Object);
            var appUserMockRep = new Mock<IApplicationUserRepository>();
            ICoursesRepository rep = new CoursesRepository(dbContext.Object, appUserMockRep.Object);

            rep.Delete(1);
            sectionPageMockSet.Verify(sp => sp.RemoveRange(), Times.Once);
            imageDataMockSet.Verify(id => id.RemoveRange(), Times.Once);
            mockSet.Verify(s => s.Remove(It.IsAny<Course>()), Times.Once());
            dbContext.Verify(s => s.SaveChanges(), Times.Once());
        }

        [Test]
        public void TestGetPublicCourses() 
        {
            var courses = new List<Course>
            {
                new Course
                {
                    IsPrivate = false,
                    EndDate = DateTime.Now.AddDays(1),
                },
                new Course
                {
                    IsPrivate = false,
                    EndDate = DateTime.Now.AddDays(1),
                },
                new Course
                {
                    IsPrivate = true,
                    EndDate = DateTime.Now.AddDays(1),
                }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Course>>();
            mockSet.As<IQueryable<Course>>().Setup(m => m.Provider).Returns(courses.Provider);
            mockSet.As<IQueryable<Course>>().Setup(m => m.Expression).Returns(courses.Expression);
            mockSet.As<IQueryable<Course>>().Setup(m => m.ElementType).Returns(courses.ElementType);
            mockSet.As<IQueryable<Course>>().Setup(m => m.GetEnumerator()).Returns(courses.GetEnumerator());

            var dbContext = new Mock<ApplicationDbContext>();
            dbContext.Setup(dc => dc.Courses).Returns(mockSet.Object);
            var appUserRep = new Mock<IApplicationUserRepository>();
            var courseRep = new CoursesRepository(dbContext.Object, appUserRep.Object);
            var usrs = courseRep.GetPublicCourses();
            Assert.That(usrs.Count(), Is.EqualTo(2));
        }
    }
}