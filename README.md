# Poodle-e-Learning platform
ASP.NET Core Web App (MVC), HTML&CSS, Bootstrap, JS, EF Core, MSSQL

## Name
Poodle-e-Learning platform

## Description

Your task is to design the new system that will be used by Telerik Academy trainers to deliver online learning content.  

The system consists of three main entities – Users (either teacher or student), Courses (either public or private) and Sections 

Teachers can create courses, add sections to them and optionally enroll students. 

Students can browse available courses and view their content 



## Authors and acknowledgment
Bozhidar Marinov and Zahari Ninov
